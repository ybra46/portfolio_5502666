# portfolio_5502666
5502666


portfolio5502666.web.app
portfolio5502666.firebaseapp.com


yasmina.bouraauan@mnd.thm.de

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Description

Willkommen auf dem Discord-Server Hub! Hier finden Sie eine Auswahl von Discord-Servern, die von mir erstellt wurden und zu verschiedenen Interessengebieten gehören. Tauchen Sie ein in lebendige Communitys, knüpfen Sie neue Kontakte und entdecken Sie spannende Themenbereiche. Unsere Server decken eine Vielzahl von Interessen ab, von Gaming über Kunst bis hin zu Musik und vielem mehr.

##Ausführungs

Das Ziel dieses Praktikums ist die Einrichtung einer voll funktionsfähigen Entwicklungsumgebung für Flutter sowie die
korrekte Einrichtung eines Git-Repositories, zu dem der Dozen eingeladen wird. Dies dient als Grundlage für die
zukünftige Arbeit und Bewertung im Rahmen des Kurse.


Installiere Flutter auf Deinem Computer. Folge der offiziellen Installationsanleitung auf der Flutter Webseite.
Stelle sicher, dass die Flutter-Umgebung richtig konfiguriert ist, indem Du den Befehl flutter doctor im
Terminal ausführst. Korrigiere eventuelle Fehler oder fehlende Komponenten, bis der Befehl keine relevanten
Fehler mehr anzeigt.



Erstelle ein neues Repository auf GitHub, GitLab oder einer ähnlichen Plattform. Der Name des Repositories muss
portfolio_<matrnr> sein, wobei <matrnr> durch Deine Matrikelnummer zu ersetzen ist.
Lade mich per E-Mail in Dein Git-Repository ein. Verwende dazu die E-Mail-Adresse des Dozenten.


Füge im Hauptverzeichnis Deines Repositories eine README.md Datei hinzu. Diese sollte eine kurze
Beschreibung des Projektes sowie Anweisungen zur Installation und Ausführung enthalten.


Initialisiere innerhalb des Repositories ein neues Flutter-Projekt mit dem Namen portfolio_<matrnr> .
Verwende dazu den Befehl:
flutter create portfolio_<matrnr>



## Documentation


Dieses Flutter-Projekt ist eine Portfolio-Anwendung, die verschiedene Seiten umfasst, darunter eine "Über mich"-Seite und eine Seite mit Links zu verschiedenen Discord-Servern. 

Die Anwendung ist responsiv gestaltet und passt sich an verschiedene Bildschirmgrößen an. 
Die wichtigsten Komponenten des Projekts sind in der pubspec.yaml-Datei definiert, die die Abhängigkeiten und Metadatees Projekts enthält.

Die Hauptdateien in diesem Projekt sind about_page.dart und servers_page.dart. Die about_page.dart-Datei beschreibt eine Seite, auf der Informationen über den Ersteller der Anwendung angezeigt werden. Diese Seite verwendet LayoutBuilder, um die Darstellung je nach Bildschirmgröße anzupassen, und enthält eine Navigationsleiste sowie eine Seitenleiste. In verschiedenen Abschnitten werden Themen wie Sprachkenntnisse und besondere Fähigkeiten behandelt. Zudem werden FontAwesomeIcons für soziale Medien-Symbole verwendet, die auf externe Links verweisen. Die servers_page.dart-Datei zeigt eine Liste von Karten, die jeweils einen Discord-Server beschreiben. Jede Karte enthält einen Titel, eine Beschreibung und einen Button, der den Benutzer zum entsprechenden Discord-Server weiterleitet. Hierbei wird url_launcher verwendet, um externe Links zu öffnen.

Für die Navigation in der Anwendung gibt es zwei Hauptkomponenten: NavbarDesktop und NavbarResponsive. NavbarDesktop ist eine Navigationsleiste für größere Bildschirme, während NavbarResponsive für kleinere Bildschirme gedacht ist und ein Menü-Symbol enthält, das eine Seitenleiste öffnet. Diese Komponenten sorgen dafür, dass die Benutzeroberfläche auf verschiedenen Geräten konsistent bleibt. Die Sidebar ist eine zusätzliche Navigationsleiste, die auf kleineren Bildschirmen durch das Menü-Symbol in der NavbarResponsive aktiviert wird.

Detaillierte Beschreibung der Hauptdateien
pubspec.yaml
Die pubspec.yaml-Datei ist das Herzstück der Projektkonfiguration. Sie definiert die Abhängigkeiten, Metadaten und andere Konfigurationen des Projekts. Zu den Hauptinhalten gehören der Projektname und die Beschreibung, die SDK-Version, die Abhängigkeiten wie url_launcher und font_awesome_flutter, sowie die Assets und Schriftarten, die in der Anwendung verwendet werden.


about_page.dart
Die AboutPage ist eine StatefulWidget-Klasse, die eine Seite darstellt, auf der persönliche Informationen über den Ersteller der Anwendung angezeigt werden. Diese Seite enthält Abschnitte zu Sprachkenntnissen und besonderen Fähigkeiten und verwendet FontAwesomeIcons für Links zu sozialen Medien. Die Darstellung passt sich je nach Bildschirmgröße an, was durch die Verwendung von LayoutBuilder ermöglicht wird. Die Seite enthält außerdem eine Navigationsleiste und eine Seitenleiste für die Navigation.

Wichtige Methoden in dieser Datei sind void _launchURL(String url), die eine angegebene URL in einem externen Browser öffnet, und Widget _buildSection(String header, String text), die einen Abschnitt mit einem Header und Text in einer Karte erstellt.


servers_page.dart
Die ServersPage ist eine StatelessWidget-Klasse, die eine Liste von Discord-Servern anzeigt. Jede Karte auf dieser Seite enthält eine Beschreibung des Servers und einen Link, um dem Server beizutreten. url_launcher wird verwendet, um URLs in einem externen Browser zu öffnen. Eine wichtige Methode in dieser Datei ist void _launchURL(String url), die eine angegebene URL in einem externen Browser öffnet.


NavbarDesktop und NavbarResponsive
NavbarDesktop zeigt Navigationslinks für größere Bildschirme und wird angezeigt, wenn die Bildschirmbreite größer als eine bestimmte Schwelle ist. NavbarResponsive hingegen zeigt ein Menü-Symbol für kleinere Bildschirme und öffnet eine Seitenleiste (Sidebar) bei Klick auf das Menü-Symbol. Diese Komponenten stellen sicher, dass die Benutzeroberfläche auf verschiedenen Geräten konsistent bleibt.



Sidebar
Die Sidebar ist eine zusätzliche Navigationsleiste, die auf kleineren Bildschirmen durch das Menü-Symbol in der NavbarResponsive aktiviert wird. Sie kann zusätzliche Navigationsoptionen oder Informationen enthalten und verbessert die Benutzerfreundlichkeit auf mobilen Geräten.

Bild hinzufügen: Hier könnte ein Screenshot der geöffneten Seitenleiste eingefügt werden, um zu zeigen, wie die zusätzliche Navigation auf mobilen Geräten funktioniert.
