import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:portfolio_5502666/widgets/navbar_desktop.dart';
import 'package:portfolio_5502666/widgets/navbar_responsive.dart';
import 'package:portfolio_5502666/widgets/sidebar.dart';

class ServersPage extends StatelessWidget {
  const ServersPage({Key? key}) : super(key: key);

  void launchDiscord(String url) async {
    final Uri uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

    return LayoutBuilder(builder: (context, constraints) {
      return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Color.fromARGB(31, 209, 154, 223),
        endDrawer: const Sidebar(),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: [
            if (constraints.maxWidth > 600)
              const NavbarDesktop()
            else
              NavbarResponsive(
                onMenuTap: () {
                  _scaffoldKey.currentState?.openEndDrawer();
                },
              ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  Card(
                    child: ListTile(
                      title: const Text('HardwareDealz Server'),
                      subtitle: const Text(
                          'This is the official server of the popular YouTube channel HardwareDealz. Here you will find everything about PC building, tech news, and hardware reviews. Exchange ideas with other tech enthusiasts, get help with your PC projects, and join exciting discussions.'),
                      trailing: const Icon(Icons.keyboard_arrow_right),
                      onTap: () {
                        launchDiscord('https://discord.com/invite/hardwaredealz');
                      },
                    ),
                  ),
                  Card(
                    child: ListTile(
                      title: const Text('Lofi Musik Server'),
                      subtitle: const Text(
                          'A relaxed place to listen to Lofi music. Perfect for studying, working, or just relaxing. Here you can create playlists with friends, stream music, and discover new tracks. Join our cozy community and enjoy the chill vibes.'),
                      trailing: const Icon(Icons.keyboard_arrow_right),
                      onTap: () {
                        launchDiscord('https://discord.com/invite/lofimusik');
                      },
                    ),
                  ),
                  Card(
                    child: ListTile(
                      title: const Text('Anime Server'),
                      subtitle: const Text(
                          'A server for all anime lovers. Discuss the latest episodes, share your favorite anime, and discover new shows. Additionally, we offer insights into the world of anime producers, with exclusive interviews and background information about your favorite series.'),
                      trailing: const Icon(Icons.keyboard_arrow_right),
                      onTap: () {
                        launchDiscord('https://discord.com/invite/anime');
                      },
                    ),
                  ),
                  Card(
                    child: ListTile(
                      title: const Text('Streamer Server'),
                      subtitle: const Text(
                          'A dedicated server for fans of a popular streamer. Meet other fans, participate in exclusive events, and get the latest updates directly from the streamer. Here you can share clips, discuss past streams, and be part of an engaged community.'),
                      trailing: const Icon(Icons.keyboard_arrow_right),
                      onTap: () {
                        launchDiscord('https://discord.com/invite/streamer');
                      },
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.facebook),
                    iconSize: 40.0,
                    onPressed: () {
                      launchDiscord('https://www.facebook.com/yasminamebel/');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.instagram),
                    iconSize: 40.0,
                    onPressed: () {
                      launchDiscord('https://www.instagram.com/yasminarebell/');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.github),
                    iconSize: 40.0,
                    onPressed: () {
                      launchDiscord('https://git.thm.de/ybra46/portfolio_5502666');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.discord),
                    iconSize: 40.0,
                    onPressed: () {
                      launchDiscord('https://discord.gg/8JpGaBsJ9h');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.envelope),
                    iconSize: 40.0,
                    onPressed: () {
                      launchDiscord('mailto:Yasminaa2003@gmail.com');
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      );
    });
  }
}
