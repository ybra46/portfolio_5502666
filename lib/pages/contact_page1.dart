import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:portfolio_5502666/widgets/navbar_desktop.dart';
import 'package:portfolio_5502666/widgets/navbar_responsive.dart';
import 'package:portfolio_5502666/widgets/sidebar.dart';

class ContactPage extends StatefulWidget {
  const ContactPage({super.key});

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  final _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _messageController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _messageController.dispose();
    super.dispose();
  }

  void _submitForm() {
    if (_formKey.currentState!.validate()) {
      final name = _nameController.text;
      final email = _emailController.text;
      final message = _messageController.text;

      // Hier kannst du die Formulardaten verarbeiten (z.B. an einen Server senden)
      print('Name: $name');
      print('Email: $email');
      print('Message: $message');

      // Zurücksetzen des Formulars nach dem Absenden
      _formKey.currentState!.reset();
    }
  }

  Future<void> _launchURL(String url) async {
    final Uri uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

    return LayoutBuilder(builder: (context, constraints) {
      return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Color.fromARGB(31, 209, 154, 223),
        endDrawer: const Sidebar(),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: [
            if (constraints.maxWidth > 600)
              const NavbarDesktop()
            else
              NavbarResponsive(
                onMenuTap: () {
                  _scaffoldKey.currentState?.openEndDrawer();
                },
              ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Contact Form',
                      style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _nameController,
                      decoration: const InputDecoration(labelText: 'Name'),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your name';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _emailController,
                      decoration: const InputDecoration(labelText: 'Email'),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter your email';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _messageController,
                      decoration: const InputDecoration(labelText: 'Message'),
                      maxLines: 5,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter a message';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 20),
                    Center(
                      child: ElevatedButton(
                        onPressed: _submitForm,
                        child: const Text('Submit'),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // Social Media Icons
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.facebook),
                    iconSize: 40.0,
                    onPressed: () {
                      _launchURL('https://www.facebook.com/yasminamebel/');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.instagram),
                    iconSize: 40.0,
                    onPressed: () {
                      _launchURL('https://www.instagram.com/yasminarebell/');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.github),
                    iconSize: 40.0,
                    onPressed: () {
                      _launchURL('https://git.thm.de/ybra46/portfolio_5502666');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.discord),
                    iconSize: 40.0,
                    onPressed: () {
                      _launchURL('https://discord.gg/8JpGaBsJ9h');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.envelope),
                    iconSize: 40.0,
                    onPressed: () {
                      _launchURL('mailto:Yasminaa2003@gmail.com');
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      );
    });
  }
}
