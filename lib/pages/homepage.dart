import 'package:flutter/material.dart';
//import 'package:portfolio_5502666/about_page.dart';
//import 'package:portfolio_5502666/contact_page.dart';
// import 'package:portfolio_5502666/servers_page.dart';
import 'package:portfolio_5502666/widgets/start_desktop.dart';
import 'package:portfolio_5502666/widgets/navbar_desktop.dart';
import 'package:portfolio_5502666/widgets/navbar_responsive.dart';
import 'package:portfolio_5502666/widgets/sidebar.dart';
import 'package:portfolio_5502666/widgets/start_responsive.dart';
import 'package:portfolio_5502666/widgets/projects.dart';
import 'package:portfolio_5502666/widgets/projects_responsive.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return Scaffold(
        key: _scaffoldKey,
        backgroundColor: const Color.fromARGB(31, 36, 51, 232),
        endDrawer: const Sidebar(),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: [
            if (constraints.maxWidth > 600)
              const NavbarDesktop()
            else
              NavbarResponsive(
                onMenuTap: () {
                  _scaffoldKey.currentState?.openEndDrawer();
                },
              ),
            // Start

            if (constraints.maxWidth > 600)
              const StartDesktop()
            else
              const StartResponsive(),
            // about me
            //const AboutPage(),

            // Skills
            if (constraints.maxWidth > 600)
              const Projects()
            else
              const ProjectsResponsive(),

            // Projects
            Container(
              height: 500,
              width: double.maxFinite,
              color: const Color.fromARGB(255, 255, 255, 255),
            ),

            // ka
            // const ServersPage(),
            // Contact
            //const ContactPage(),
          ],
        ),
      );
    });
  }
}