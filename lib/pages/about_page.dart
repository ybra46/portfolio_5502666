import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:portfolio_5502666/widgets/navbar_desktop.dart';
import 'package:portfolio_5502666/widgets/navbar_responsive.dart';
import 'package:portfolio_5502666/widgets/sidebar.dart';

class AboutPage extends StatefulWidget {
  const AboutPage({super.key});

  @override
  State<AboutPage> createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> _launchURL(String url) async {
    final Uri uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {

    const String aboutHeader = 'About Me';
    const String aboutText =
        'Hello! My name is Yasmina and I am the creator of these Discord servers.\n'
        'I am interested in many different topics and I am excited to share these interests with you.';

    const String languageSkillsHeader = 'Language Skills';
    const String languageSkillsText =
        'Native language: Berber, fluent\n'
        'Second language: German, fluent\n'
        'Foreign languages: English (school level), French (basic knowledge and text comprehension), Arabic (reading and writing), Korean (reading and writing)';

    const String specialSkillsHeader = 'Special Skills';
    const String specialSkillsText =
        'IT skills: Computer knowledge in software & hardware\n'
        'Word, PowerPoint, Excel, and Prezi skills\n'
        'C# / Java object-oriented programming & SQL';

    return LayoutBuilder(builder: (context, constraints) {
      return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Color.fromARGB(31, 209, 154, 223),
        endDrawer: const Sidebar(),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: [
            if (constraints.maxWidth > 600)
              const NavbarDesktop()
            else
              NavbarResponsive(
                onMenuTap: () {
                  _scaffoldKey.currentState?.openEndDrawer();
                },
              ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    aboutHeader,
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 10),
                  Text(aboutText),
                ],
              ),
            ),
            _buildSection(languageSkillsHeader, languageSkillsText),
            _buildSection(specialSkillsHeader, specialSkillsText),
            // Social Media Icons
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.facebook),
                    iconSize: 40.0, // Größe des Icons
                    onPressed: () {
                      _launchURL('https://www.facebook.com/yasminamebel/');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.instagram),
                    iconSize: 40.0, // Größe des Icons
                    onPressed: () {
                      _launchURL('https://www.instagram.com/yasminarebell/');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.github),
                    iconSize: 40.0, // Größe des Icons
                    onPressed: () {
                      _launchURL('https://git.thm.de/ybra46/portfolio_5502666');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.discord),
                    iconSize: 40.0, // Größe des Icons
                    onPressed: () {
                      _launchURL('https://discord.gg/8JpGaBsJ9h');
                    },
                  ),
                  IconButton(
                    icon: FaIcon(FontAwesomeIcons.envelope),
                    iconSize: 40.0, // Größe des Icons
                    onPressed: () {
                      _launchURL('mailto:Yasminaa2003@gmail.com');
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      );
    });
  }

  Widget _buildSection(String header, String text) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
      child: Card(
        elevation: 4.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                header,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 10),
              Text(text),
            ],
          ),
        ),
      ),
    );
  }
}