import 'package:flutter/material.dart';

class NavbarLogo extends StatelessWidget {
  const NavbarLogo({super.key});

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Discord Server Hub',
      style: TextStyle(
          color: Color.fromARGB(255, 255, 255, 255),
          fontSize: 24,
          fontWeight: FontWeight.bold,
          decoration: TextDecoration.underline,
          decorationColor: Color.fromARGB(255, 255, 255, 255)),
    );
  }
}
