import 'package:flutter/material.dart';
import 'package:portfolio_5502666/pages/homepage.dart';
import 'package:portfolio_5502666/widgets/navbar_logo.dart';

import '../pages/about_page.dart';
import '../pages/contact_page1.dart';
import '../pages/projects_page.dart';

class NavbarDesktop extends StatelessWidget {
  const NavbarDesktop({super.key});

  @override
  Widget build(BuildContext context) {
    return 
        Container(
      height: 50,
      margin: const EdgeInsets.symmetric(vertical: 0, horizontal: 0),
      padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 30),
      width: double.maxFinite,
      color: Color.fromARGB(255, 93, 51, 109), //obere Zeile Frabe
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const NavbarLogo(),
          const Spacer(),
          TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const HomePage()),
                );
              },
              child: const Text(
                'Home',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.normal,
                    color: Color.fromARGB(255, 255, 255, 255)),
              )),
          TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const AboutPage()),
                );
              },
              child: const Text(
                'About me',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.normal,
                    color: Color.fromARGB(255, 255, 255, 255)),
              )),
          TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ServersPage()),
                );
              },
              child: const Text(
                'Projects',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.normal,
                    color: Color.fromARGB(255, 255, 255, 255)),
              )),
          
          
          
          TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const ContactPage()),
                );
              },
              child: const Text(
                'Contact',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.normal,
                    color: Color.fromARGB(255, 255, 255, 255)),
              )),

        ],
      ),
    );
  }
}
