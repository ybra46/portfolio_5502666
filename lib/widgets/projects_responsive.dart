import 'package:flutter/material.dart';

class ProjectsResponsive extends StatelessWidget {
  const ProjectsResponsive({super.key});

  @override
  Widget build(BuildContext context) {
    final projects = [
      {'name': 'Hardware', 'image': 'assets/image.webp', 'link': 'https://discord.com/invite/scXpUQjS'},
      {'name': 'Lofi Girl', 'image': 'assets/image (2).webp','link': 'https://discord.com/invite/scXpUQjS'},
      {'name': 'Aniground', 'image': 'assets/image (1).webp','link': 'https://discord.com/invite/scXpUQjS'},
      {'name': 'Stremer Server', 'image': 'assets/MURAI-Logo.webp','link': 'https://discord.com/invite/scXpUQjS'},
    ];

    return Container(
      padding: const EdgeInsets.all(16.0),
      color: Color.fromARGB(255, 0, 0, 0), // Schöne dunkelblaue Hintergrundfarbe
      child: Center(
        child: Card(
          color: const Color.fromARGB(
              255, 28, 40, 51), // Etwas hellere Dunkelblau für die Karte
          elevation: 4.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text(
                  'My projects',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(height: 20),
                GridView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    crossAxisSpacing: 2.0,
                    mainAxisSpacing: 2.0,
                  ),
                  itemCount: projects.length,
                  itemBuilder: (context, index) {
                    final skill = projects[index]; //TODO
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          skill['image']!,
                          height: 50,
                          width: 50,
                        ),
                        const SizedBox(height: 8),
                        Text(
                          skill['name']!,
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
