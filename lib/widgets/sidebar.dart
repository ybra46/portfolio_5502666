import 'package:flutter/material.dart';
import 'package:portfolio_5502666/pages/homepage.dart';
import 'package:portfolio_5502666/pages/about_page.dart';
import 'package:portfolio_5502666/pages/contact_page1.dart';

import 'package:portfolio_5502666/pages/projects_page.dart';

class Sidebar extends StatelessWidget {
  const Sidebar({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Color.fromRGBO(31, 36, 51, 1.0),
      child: ListView(
        children: [                                                                                                      
          const SizedBox(
            height: 50,
          ),
          ListTile(
            title: const Text('Home',
                style: TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255),
                  fontSize: 16,
                  fontWeight: FontWeight.normal,
                )),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const HomePage()),
              );
            },
          ),
          ListTile(
            title: const Text('About me',
                style: TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255),
                  fontSize: 16,
                  fontWeight: FontWeight.normal,
                )),
           onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const AboutPage()),
              );
            },
          ),
            ListTile(
            title: const Text('Projekts',
                style: TextStyle(
                  color: Color.fromARGB(255, 255, 255, 255),
                  fontSize: 16,
                  fontWeight: FontWeight.normal,
                )),
           onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const ServersPage()),
              );
            },
          ),

          ListTile(
            title: const Text('Kontakt',
                style: TextStyle(color: Colors.white)),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const ContactPage()),
              );
            },
          ),
  
          Padding(
            padding: const EdgeInsets.all(20),
            child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(
                  Icons.close_sharp,
                  color: Colors.white,
                )),
          ),
        ],
      ),
    );
  }
}