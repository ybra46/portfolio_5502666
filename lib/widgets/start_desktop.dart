import 'package:flutter/material.dart';

class StartDesktop extends StatelessWidget {
  const StartDesktop({super.key});

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final screenWidth = screenSize.width;
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      height: screenSize.height * 0.5,
      constraints: BoxConstraints(
        minHeight: 350,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          const Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Yasmina Bouraauan",
                style: TextStyle(
                  color: Color.fromARGB(255, 163, 74, 172),
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
          Image.asset(
            "assets/Discord-Photoroom.png",
            width: screenWidth / 2,
          ),
        ],
      ),
    );
  }
}
