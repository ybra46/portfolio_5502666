import 'package:flutter/material.dart';
import 'package:portfolio_5502666/pages/about_page.dart';
import 'package:portfolio_5502666/pages/homepage.dart';
import 'package:portfolio_5502666/pages/contact_page1.dart';
import 'package:portfolio_5502666/pages/projects_page.dart';

//import 'servers_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Discod Server Hub',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        initialRoute: '/',
        routes: { 
          '/': (context) => const HomePage(),
          'about': (context) => const AboutPage(),
          'projects': (context) => const ServersPage(),
          'contact': (context) => const ContactPage(),

          
        });
  }
}
